# Intro
Sorry for the python devs purist, as a Devops Engineer with heavy Sysadmin background. I'm more an ops than a dev. So please, forgive me if the PEP8 is not well applied here.

# What is it ?
It's a little program aims to make automated Hashicorp Vault snapshots through the Vault api and expose metrics in order to be able to monitor them with a Prometheus server.

# Why this program ?
Because currently, only Vault Enterprise(paid version) permits to do what this little program does (automated snapshot and monitoring).

# Features
- Multithreaded, each section of the config file can be a completely different Vault server.
- Expose Prometheus Metrics, currently two gauge metrics exists:  
vault_oss_snapshots_[save_errors|last_success]
- Can be used as daemon, if snapshot frequency(in minute) is set or as oneshot script(no monitoring in this case, but useful if you use it with cronjob).
- Upload snapshots to an S3 object storage.
- Snapshot rotation(remove old snapshots older than n minutes).

# How To Use
Suit the config file (vault_snapshot_exporter.conf) to your needs and run with either Kubernetes, or from any Linux OS with Python >=3.10.
## Kubernetes
kube-prometheus-stack is needed or remove the podmonitor manifest in kustomization.yaml.  
```bash
kubectl apply -k ./  
```
## Docker
```bash
docker run -v ${PWD}:/conf registry.gitlab.com/arnaudg-stuff/vault/vault-snapshot-exporter:latest -c /conf/vault_snapshot_exporter.conf
```
## From any Linux OS with Python3 Interpreter
```bash
pip install -r requirements.txt  
python3 vault_snapshot_exporter.py -c vault_snapshot_exporter.conf
```
