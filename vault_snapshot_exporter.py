import requests,json,argparse,os,configparser,re,tarfile,sys,socket,ssl,boto3,time,logging,threading
from botocore.exceptions import ClientError
from datetime import datetime
from prometheus_client import start_http_server, Gauge
from argparse import RawTextHelpFormatter
from configparser import ConfigParser, ExtendedInterpolation

## Disable all unused defaults metrics
import prometheus_client
prometheus_client.REGISTRY.unregister(prometheus_client.GC_COLLECTOR)
prometheus_client.REGISTRY.unregister(prometheus_client.PLATFORM_COLLECTOR)
prometheus_client.REGISTRY.unregister(prometheus_client.PROCESS_COLLECTOR)
####

logging.basicConfig(format=f'%(levelname)s {sys.argv[0]}[%(process)d] %(asctime)s %(message)s', datefmt='%m/%d/%Y %H:%M:%S',level=logging.INFO)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description='Vault snapshot script',formatter_class=RawTextHelpFormatter)
parser.add_argument('--server', '-S', help='Vault server to snap (it must be a full URI : https://<host>:<port>)', metavar='server', required=False)
parser.add_argument('--no-check','-n', help='Skip check socket availability, before requests, useful behind proxy', action='store_false')
parser.add_argument('--tls-verify','-N', help='Skip tls certs verification', action='store_false')
parser.add_argument('--role-id', '-r', help='Approle Role Id', metavar='role_id', required=False)
parser.add_argument('--secret-id', '-s', help='Approle Secret Id', metavar='secret_id', required=False)
parser.add_argument('--token', '-t', help='Vault Token, Default to VAULT_TOKEN Var', metavar='token', required=False, default=os.getenv('VAULT_TOKEN'))
parser.add_argument('--s3-access-key', '-ak', help='If not set AWS var by default or ~/.aws/credentials', metavar='access_key',required=False,default=os.getenv('AWS_ACCESS_KEY_ID'))
parser.add_argument('--s3-secret-key', '-sk', help='If not set AWS var by default or ~/.aws/credentials', metavar='secret_key',required=False,default=os.getenv('AWS_SECRET_ACCESS_KEY'))
parser.add_argument('--s3-url', '-su', help='If not set AWS var by default or ~/.aws/config', metavar='s3_url',required=False,default=os.getenv('AWS_ENDPOINT_URL'))
parser.add_argument('--s3-region', '-sr', help='If not set AWS var by default or ~/.aws/config', metavar='s3_region',required=False,default=os.getenv('AWS_REGION_NAME'))
parser.add_argument('--bucket', '-b', help='Upload snapshot to S3, AWS S3 params must be set', metavar='bucket', required=False, default=None)
parser.add_argument('--path', '-p', help='Snapshot local path, default: current dir', metavar='path', default=os.curdir, required=False)
parser.add_argument('--monitor', '-m', help='Expose metrics:\
                                            --frequency parameter must be set\
                                           \nMetrics: vault_oss_snapshots_[save_errors|last_success]\n\n',\
                                           metavar='monitor_server:port', default=None, required=False)

parser.add_argument('--filename', '-f', help='Snapshot file name prefix, default: vault_oss-snapshot', metavar='filename', default='vault-backup', required=False)
parser.add_argument('--delete-time', '-d', help='Delete locally snapshots older than n minutes ago', metavar='delete_time',required=False,type=int)
parser.add_argument('--frequency', '-F', help='Backup every n minutes', metavar='backup_frequency', default=None,required=False,type=int)
parser.add_argument('--config', '-c', help='Load options from config file', metavar='config', required=False)
args = parser.parse_args()

arch_default_cont={'meta.json', 'state.bin', 'SHA256SUMS', 'SHA256SUMS.sealed'}

# collect proxy envs
proxy = os.getenv('http_proxy') or os.getenv('https_proxy')

if proxy:
  proxies = { "http": proxy,
              "https": proxy
            }
else:
  proxies = None

# define metrics's labels
labels_failed = ["cluster","id"]
labels_success = ["cluster","id","snapshot_name","bucket"]

def read_config(configfile):
  config = configparser.ConfigParser(default_section='global',allow_no_value=True,
           defaults=None,interpolation=ExtendedInterpolation())
  try:
    config.read(configfile)
  except TypeError:
    config = None
  return config

conf = read_config(args.config)

if conf:
  monitor = conf['global'].get('monitor',args.monitor)
else:
  monitor = args.monitor

if monitor:
  addr,port=monitor.split(':')
  start_http_server(int(port),addr)
  metric_failed=Gauge("vault_oss_snapshot_last_save_error", "Vault Last Snapshot Error",labels_failed)
  metric_success=Gauge("vault_oss_snapshot_last_success", "Vault Last Snapshot Success",labels_success)

class VaultOps:
  def __init__(self,server,**kwargs):
    try:
      self.instance_id = kwargs.get('instance_id') or None
    except Exception as e:
      logger.warning(f'There is no instance id defined, {e}')
    if (kwargs.get('token') or (kwargs.get('role') and kwargs.get('secret'))) and server:
      self.server = server
    else:
      parser.print_help()
      logger.error('vault server and credentials(token or approle) must be set!')
      exit(1)
    self.tls_verify = kwargs.get('tls_verify')
   # testing the socket availability
    self.no_check = kwargs.get('no_check')
    # behind proxy, it's better to disable the socket check
    if not self.no_check:
      try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(5)
        url_split=re.match('(https?)://(.*)',self.server)
        # set port to 443 or 80 if not specified
        if url_split.group(1) == 'http' and len(str(url_split.group(2)).split(':')) < 2:
          port=80
        elif url_split.group(1) == 'https' and len(str(url_split.group(2)).split(':')) < 2:
          port=443
        else:
          port=int(str(url_split.group(2)).split(':')[1])
        address=str(url_split.group(2)).split(':')[0]
        if url_split.group(1) == 'https':
          context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
          if not self.tls_verify:
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE
          context.load_default_certs()
          ssl_sock=context.wrap_socket(sock,server_hostname=address)
          ssl_sock.connect((address,port))
        else:
          sock.connect((address,port)) 
        sock.close()
      except Exception as e:
        logger.error(f'Error while connecting to {self.server}, {e}')
        sys.exit(1)
    self.token = kwargs.get('token')
    if not self.token:
      payload = json.dumps({'role_id': f'{kwargs.get("role")}', 'secret_id': f'{kwargs.get("secret")}'})
      r = requests.post(f'{self.server}/v1/auth/approle/login',
          data=payload,
          allow_redirects=True,verify=self.tls_verify,proxies=proxies
               )
      if r.status_code == 200:
        http_data = r.json()
        self.token = http_data['auth']['client_token']
      else:
        logger.error('Cannot get token ! ', r.status_code)
        sys.exit(1)
  
  def __clustername(self):
    token={ "X-Vault-Token": self.token }
    r = requests.get(f'{self.server}/v1/sys/health?standbyok=true&perfstandbyok=true',headers=token,
        allow_redirects=True,verify=self.tls_verify, proxies=proxies
             )
    if r.status_code == 200:
      self.clustername = r.json()['cluster_name']
    else:
      logger.error('Cannot get clustername ! ', r.status_code)
      sys.exit(1)

  def __clean_timeseries(self,metric):
    for label in metric.collect()[0].samples:
      #if self.instance_id in label.labels.values():
      if str(self.instance_id) == label.labels['id']:
        metric.remove(*label.labels.values())
  
  def __snap_monitor(self,status):
    if monitor:
      if status == 'ko':
        self.__clean_timeseries(metric_failed)
        metric_failed.labels(self.server,self.instance_id).set(1)
      elif status == 'ok':
        self.__clean_timeseries(metric_success)
        metric_success.labels(self.server,self.instance_id,str(self.snap).split('/')[-1],str(self.bucket)).set(1)
        self.__clean_timeseries(metric_failed)
        metric_failed.labels(self.server,self.instance_id).set(0)

  def __snap_failed(self,retry,failed,first_try):
    if first_try and not failed:
      failed = True
    elif retry != 0 and failed:
      logger.error('retrying in 5 seconds')
      time.sleep(5)
    else:
      failed = False
    return failed

  def snapshot(self,**kwargs):
    self.path = kwargs.get('path')
    self.filename = kwargs.get('filename')
    self.s3_access_key = kwargs.get('s3_access_key')
    self.s3_secret_key = kwargs.get('s3_secret_key')
    self.s3_url = kwargs.get('s3_url')
    self.s3_region = kwargs.get('s3_region')
    self.bucket = kwargs.get('bucket')
    self.__clustername()
    while True:
      retry = 5
      failed = False
      first_try = True
      while self.__snap_failed(retry,failed,first_try):
        global ts
        ts = datetime.now().strftime('%s')
        self.snap = f'{self.path}/{self.filename}-{ts}.snap'
        r = requests.get(f'{self.server}/v1/sys/storage/raft/snapshot',
            headers={'X-Vault-Token': self.token},verify=self.tls_verify,
            proxies=proxies,allow_redirects=True
                 )
        if r.status_code == 200:
            # Check integrity of each file in the archive
          try:
            with open(self.snap,mode='bx') as snap:
              snap.write(r.content)
            try:
              snapshot_arch = tarfile.open(self.snap,'r:gz')
              for member in snapshot_arch.getmembers():
                with snapshot_arch.extractfile(member.name) as target:
                  for chunk in iter(lambda: target.read(1024), b''):
                    pass
              # Check Default archive content
              try:
                members_arch=iter(snapshot_arch.getnames())
                for i in arch_default_cont:
                  arch_default_cont.add(str(next(members_arch)))
                logger.info(f'ID[{self.instance_id}] Snapshot saved successfully!')
                if self.bucket:
                  if self.__upload_snap(self.snap):
                    logger.info(f'ID[{self.instance_id}] Snapshot sent successfully to the S3 bucket {self.bucket}')
                  else:
                    logger.warning(f'ID[{self.instance_id}] Snapshot sent failed to the S3 bucket {self.bucket}')
                self.__snap_monitor('ok')
                failed = False
                first_try = False
              except(StopIteration,RuntimeError) as e:
                logger.error(f'ID[{self.instance_id}] Snapshot Incomplete, Errors',e,'\nDeleting the broken snapshot')
                os.remove(self.snap)
                self.snap = None
                self.__snap_monitor('ko')
                failed = True
                retry-=1
              snapshot_arch.close()
            except Exception as e:
              logger.error(f'ID[{self.instance_id}] Error while checking snapshot integrity',e,'\nDeleting the broken snapshot')
              os.remove(self.snap)
              self.snap = None
              self.__snap_monitor('ko')
              failed = True
              retry-=1
            if kwargs['deltime'] and self.snap != None:
              regex=re.compile(f'({self.filename}-)([0-9]*)(\.snap)$')
              #regex=re.compile('(.*-)([0-9]*)(\.snap)$')
              snaplist=[]
              for f in os.listdir(self.path):
                if regex.search(f):
                  snaplist.append(regex.search(f).group())
                  for snaptodel in snaplist:
                    total=(datetime.fromtimestamp(int(ts)) - datetime.fromtimestamp(int(regex.search(snaptodel).group(2))))
                    if int(total.total_seconds()) > int(kwargs['deltime'])*60:
                      logger.info(f'ID[{self.instance_id}] Now deleting snapshot {snaptodel} older than {kwargs["deltime"]} min')
                      os.remove(f'{self.path}/{snaptodel}')
                    snaplist.remove(snaptodel)
          except IOError as e:
            logger.error(f'ID[{self.instance_id}] Failed to save snapshot on the disk !', e)
            self.__snap_monitor('ko')
            failed = True
            retry-=1
        else:
          logger.error(f'ID[{self.instance_id}] Getting snapshot failed ! http error : {r.status_code}')
          self.__snap_monitor('ko')
          failed = True
          retry-=1
      if kwargs.get('frequency'):
        time.sleep(kwargs['frequency']*60)
      else:
        sys.exit(0)

  # Finally revoke token
    try:
      r=requests.post(f'{self.server}/v1/auth/token/revoke-self',
          headers={'X-Vault-Token': self.token},
          allow_redirects=True,verify=self.tls_verify,proxies=proxies
              )
    except Exception as e:
      logger.warning(f"ID[{self.instance_id}] No token Revoked !", e)

  def __upload_snap(self, file_name, object_name=None):
    if object_name is None:
        object_name = os.path.basename(file_name)
  
    # Upload the file
    s3_client = boto3.client('s3',aws_access_key_id=self.s3_access_key,
                             aws_secret_access_key=self.s3_secret_key,
                             endpoint_url=self.s3_url,
                             region_name=self.s3_region)
    try:
        response = s3_client.upload_file(file_name, self.bucket, object_name)
    except ClientError as e:
        logger.error(e)
        return False
    return True

if __name__ == "__main__":
  if conf:
    jobs = []
    for section in conf:
      try:
        if section == 'global':
          continue
        server = conf[section].get('server')
        role = conf[section].get('role-id')
        secret = conf[section].get('secret-id')

        # Override some defaults option with args cli and vars
        token = conf[section].get('token',args.token)
        s3_access_key = conf[section].get('s3-access-key',args.s3_access_key)
        s3_secret_key = conf[section].get('s3-secret-key',args.s3_secret_key)
        s3_url = conf[section].get('s3-url',args.s3_url)
        s3_region = conf[section].get('s3-region',args.s3_region)
        no_check = conf[section].getboolean('no-check', conf['global'].get('no-check',args.no_check))
        tls_verify = conf[section].getboolean('tls-verify', conf['global'].get('tls-verify',args.tls_verify))

        itemsdict = { "path": conf[section].get('path',os.curdir),"filename": conf[section].get('filename',f'{section}-backup'),
                     "deltime": conf[section].getint('delete-time'),"frequency": conf[section].getint('frequency'),
                     "bucket": conf[section].get('bucket'),"s3_url": s3_url, "s3_region": s3_region,"s3_access_key": s3_access_key,
                     "s3_secret_key": s3_secret_key, "instance_id": section }

        vault=VaultOps(server=server,role=role,no_check=no_check,tls_verify=tls_verify,
                       secret=secret,token=token,instance_id=section)
        vault_snapshot = threading.Thread(target=vault.snapshot,kwargs=itemsdict)
        jobs.append(vault_snapshot)
      except KeyError as e:
        logger.error(f'{e} key not found')
        sys.exit(1)
    for j in jobs:
      j.start()
    for j in jobs:
      j.join()
  else:
    itemsdict = { "path": args.path,"filename": args.filename,
                 "deltime": args.delete_time,"frequency": args.frequency,
                 "bucket": args.bucket,"s3_url": args.s3_url,
                 "s3_region": args.s3_region,"s3_access_key": args.s3_access_key,
                 "s3_secret_key": args.s3_secret_key}

    vault=VaultOps(args.server,no_check=args.no_check,tls_verify=args.tls_verify,
                   token=args.token,role=args.role_id,secret=args.secret_id)

    vault.snapshot(**itemsdict)
