FROM python:3.10.14

WORKDIR /scripts

COPY requirements.txt vault_snapshot_exporter.py vault_snapshot_exporter.conf .
RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT [ "python", "vault_snapshot_exporter.py" ]
CMD ["-c", "vault_snapshot_exporter.conf"]
